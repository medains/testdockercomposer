FROM php:7-cli

RUN apt-get update -q && \
    apt-get upgrade -qy && \
    apt-get install -qy \
      git \
      libcurl4-openssl-dev \
      zlib1g-dev 

WORKDIR /srv

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename composer
